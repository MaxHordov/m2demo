<?php
/**
 * Portofoonweb header contacts.
 */

declare(strict_types=1);

namespace Portofoonweb\Theme\Block\Html;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

/**
 * Class HeaderContacts block.
 */
class HeaderContacts extends Template
{
    /**
     * Scope Config Interface
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * HeaderContacts constructor.
     *
     * @param Context              $context
     * @param ScopeConfigInterface $scopeConfig
     * @param array                $data
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Get store phone number.
     *
     * @return mixed
     */
    public function getStorePhone()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/phone',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store work hours.
     *
     * @return mixed
     */
    public function getStoreHours()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/hours',
            ScopeInterface::SCOPE_STORE
        );
    }
}
