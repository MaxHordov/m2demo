<?php
/**
 * Portofoonweb Theme
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_Theme',
    __DIR__
);
