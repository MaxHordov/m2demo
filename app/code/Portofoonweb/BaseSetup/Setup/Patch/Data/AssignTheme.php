<?php

/**
 * Portofoonweb assign theme.
 *
 * @author Maksym Hordov <gordovmax@gmail.com>
 */

declare(strict_types=1);

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\Store;
use Magento\Theme\Model\Config;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

/**
 * Class AssignTheme
 *
 * Portofoonweb\BaseSetup\Setup\Patch\Data
 */
class AssignTheme implements DataPatchInterface
{
    /**
     * Portofoonweb theme name.
     */
    const THEME_NAME = 'frontend/Portofoonweb/default';

    /**
     * Config.
     *
     * @var \Magento\Theme\Model\Config
     */
    private $config;

    /**
     * Collection factory.
     *
     * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
     */
    private $collectionFactory;

    /**
     * AssignTheme constructor.
     *
     * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory
     * @param \Magento\Theme\Model\Config                                $config
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Config $config
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
    }

    /**
     * Apply.
     *
     * @return void
     */
    public function apply(): void
    {
        $theme = $this->collectionFactory->create()->getThemeByFullPath(static::THEME_NAME);
        $this->config->assignToStore(
            $theme,
            [Store::DEFAULT_STORE_ID,Store::DISTRO_STORE_ID]
        );
    }

    /**
     * Get dependencies.
     *
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * Get aliases.
     *
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
