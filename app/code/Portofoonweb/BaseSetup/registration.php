<?php
/**
 * Portofoonweb Setup config
 *
 * @category  Portofoonweb
 * @package   Portofoonweb\Default
 * @author    Maksym Hordov <gordovmax@gmail.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_BaseSetup',
    __DIR__
);
