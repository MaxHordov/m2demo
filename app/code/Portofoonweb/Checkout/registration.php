<?php
/**
 * Portofoonweb Checkout
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_Checkout',
    __DIR__
);
